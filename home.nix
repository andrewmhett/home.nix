{ config, pkgs, ... }:

let home_dir = if pkgs.system == "aarch64-darwin"
    then  "/Users/andrew"
    else  "/home/andrew";
in {
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "andrew";
  home.homeDirectory = home_dir;

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello

    # Useful cli tools
    pkgs.gnupg
    pkgs.lsd
    pkgs.universal-ctags
    pkgs.ranger

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    (pkgs.nerdfonts.override { fonts = [ "SourceCodePro" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';

    ".gruvbox.dircolors".text = ''
      # Below, there should be one TERM entry for each termtype that is colorizable
      TERM Eterm
      TERM ansi
      TERM color-xterm
      TERM con132x25
      TERM con132x30
      TERM con132x43
      TERM con132x60
      TERM con80x25
      TERM con80x28
      TERM con80x30
      TERM con80x43
      TERM con80x50
      TERM con80x60
      TERM cons25
      TERM console
      TERM cygwin
      TERM dtterm
      TERM dvtm
      TERM dvtm-256color
      TERM eterm-color
      TERM fbterm
      TERM gnome
      TERM gnome-256color
      TERM jfbterm
      TERM konsole
      TERM konsole-256color
      TERM kterm
      TERM linux
      TERM linux-c
      TERM mach-color
      TERM mlterm
      TERM putty
      TERM putty-256color
      TERM rxvt
      TERM rxvt-256color
      TERM rxvt-cygwin
      TERM rxvt-cygwin-native
      TERM rxvt-unicode
      TERM rxvt-unicode-256color
      TERM rxvt-unicode256
      TERM screen
      TERM screen-16color
      TERM screen-16color-bce
      TERM screen-16color-bce-s
      TERM screen-16color-s
      TERM screen-256color
      TERM screen-256color-bce
      TERM screen-256color-bce-s
      TERM screen-256color-italic
      TERM screen-256color-s
      TERM screen-bce
      TERM screen-w
      TERM screen.Eterm
      TERM screen.linux
      TERM screen.rxvt
      TERM screen.xterm-256color
      TERM st
      TERM st-256color
      TERM st-meta
      TERM st-meta-256color
      TERM terminator
      TERM tmux
      TERM tmux-256color
      TERM vt100
      TERM xterm
      TERM xterm-16color
      TERM xterm-256color
      TERM xterm-256color-italic
      TERM xterm-88color
      TERM xterm-color
      TERM xterm-debian
      TERM xterm-termite

      NORMAL 00;38;5;250 # no color code at all
      FILE   00;38;5;223 # regular file: use no color at all
      RESET  00;38;5;172 # reset to "normal" color
      DIR    00;38;5;166 # directory
      LINK   target      # symbolic link. (If you set this to 'target' instead of a
                         # numerical value, the color is as for the file pointed to.)

      MULTIHARDLINK         04        # regular file with more than one link
      FIFO                  40;33     # pipe
      SOCK                  38;5;211  # socket
      DOOR                  38;5;211  # door
      BLK                   40;33;01  # block device driver
      CHR                   40;33;01  # character device driver
      ORPHAN                40;31;01  # symlink to nonexistent file, or non-stat'able file
      SETUID                37;41     # file that is setuid (u+s)
      SETGID                30;43     # file that is setgid (g+s)
      CAPABILITY            30;41     # file with capability
      STICKY_OTHER_WRITABLE 01;36;44  # dir that is sticky and other-writable (+t,o+w)
      OTHER_WRITABLE        38;5;208;48;5;243 # dir that is other-writable (o+w) and not sticky
      STICKY                38;5;208;48;5;243 # dir with the sticky bit set (+t) and not other-writable

      # This is for files with execute permission:
      EXEC  01;30;32

      # Crufty and ignorable files
      .bak   38;5;246
      .cache 38;5;246
      .dist  38;5;246
      .lock  38;5;246
      .log   38;5;246
      .old   38;5;246
      .orig  38;5;246
      .temp  38;5;246
      .tmp   38;5;246

      # archives or compressed (bright red)
      .7z   01;31
      .Z    01;31
      .ace  01;31
      .arj  01;31
      .bz   01;31
      .bz2  01;31
      .cpio 01;31
      .deb  01;31
      .dz   01;31
      .ear  01;31
      .gz   01;31
      .jar  01;31
      .lz   01;31
      .lzh  01;31
      .lzma 01;31
      .rar  01;31
      .rpm  01;31
      .rz   01;31
      .sar  01;31
      .tar  01;31
      .taz  01;31
      .tbz  01;31
      .tbz2 01;31
      .tgz  01;31
      .tlz  01;31
      .txz  01;31
      .tz   01;31
      .war  01;31
      .xz   01;31
      .z    01;31
      .zip  01;31
      .zoo  01;31

      # image formats
      .anx  01;35
      .asf  01;35
      .avi  01;35
      .axv  01;35
      .bmp  01;35
      .cgm  01;35
      .dl   01;35
      .emf  01;35
      .flc  01;35
      .fli  01;35
      .flv  01;35
      .gif  01;35
      .gl   01;35
      .jpeg 01;35
      .jpg  01;35
      .m2v  01;35
      .m4v  01;35
      .mkv  01;35
      .mng  01;35
      .mov  01;35
      .mp4  01;35
      .mp4v 01;35
      .mpeg 01;35
      .mpg  01;35
      .nuv  01;35
      .ogm  01;35
      .ogv  01;35
      .ogx  01;35
      .pbm  01;35
      .pcx  01;35
      .pgm  01;35
      .png  01;35
      .ppm  01;35
      .qt   01;35
      .rm   01;35
      .rmvb 01;35
      .svg  01;35
      .svgz 01;35
      .tga  01;35
      .tif  01;35
      .tiff 01;35
      .vob  01;35
      .webm 01;35
      .wmv  01;35
      .xbm  01;35
      .xcf  01;35
      .xpm  01;35
      .xwd  01;35
      .yuv  01;35

      # audio formats
      .aac  01;33
      .au   01;33
      .flac 01;33
      .mid  01;33
      .midi 01;33
      .mka  01;33
      .mp3  01;33
      .mpc  01;33
      .ogg  01;33
      .ra   01;33
      .wav  01;33
      .axa  01;33
      .oga  01;33
      .spx  01;33
      .xspf 01;33

      # documents
      .doc   38;5;109
      .docx  38;5;109
      .html  38;5;109
      .js    38;5;109
      .json  38;5;109
      .md    38;5;109
      .neon  38;5;109
      .odp   38;5;109
      .ods   38;5;109
      .odt   38;5;109
      .pdf   38;5;109
      .php   38;5;109
      .phtml 38;5;109
      .ppt   38;5;109
      .pptx  38;5;109
      .tex   38;5;109
      .txt   38;5;109
      .xls   38;5;109
      .xlsx  38;5;109
      .xml   38;5;109
    '';
  };

  programs.git = {
    enable = true;
    userName = "Andrew Hett";
    userEmail = "andrewmhett@icloud.com";
    signing.key = "5BFC6AFAADA02B8F";
    signing.signByDefault = true;
  };

  programs.vim = {
    enable = true;
    extraConfig = ''
        colorscheme gruvbox

        set encoding=utf-8
        set ff=unix

        set ignorecase
        set cursorline
        set incsearch
        set showmatch
        set undodir=$HOME/.vim_undo
        silent !mkdir ~/.vim_undo > /dev/null 2>&1
        set undofile

        " Uncomment the following to have Vim jump to the last position when
        " reopening a file
        if has("autocmd")
          au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
            \| exe "normal! g'\"" | endif
        endif

        let g:ale_fixers = {
        \   '*'     : ['remove_trailing_lines', 'trim_whitespace'],
        \   'python': ['autoimport','isort','remove_trailing_lines','trim_whitespace'],
        \   'cpp'   : ['remove_trailing_lines','trim_whitespace'],
        \   'c'     : ['remove_trailing_lines','trim_whitespace'],
        \}

        let g:ale_linters = {
        \   'python': ['pylint'],
        \   'asm'   : [],
        \   'cpp'   : ['clang']
        \}

        let g:gruvbox_guisp_fallback = 'bg'
        let g:ale_fix_on_save = 1
        let g:ale_lint_on_save = 0

        let g:airline_powerline_fonts = 1
        let g:airline#extensions#tabline#enabled = 1

        let mapleader = "."
        filetype plugin on
        set updatetime=100
        set autoindent
        set nu
        set backspace=indent,eol,start
        set bg=dark
        set guifont=SauceCodePro\ Nerd\ Font\ Mono:h15
        set guioptions=
        let g:easytags_async = 1
        let g:easytags_python_enabled = 1

        if has('macunix') " iterm2 cursor shape integration
          let &t_SI.="\e[5 q" "SI = INSERT mode
          let &t_SR.="\e[4 q" "SR = REPLACE mode
          let &t_EI.="\e[1 q" "EI = NORMAL mode (ELSE)
        endif
        if has('unix')  "GNOME terminal cursor shape integration
                au VimEnter,InsertLeave * silent execute '!echo -ne "\e[1 q"'
                au InsertEnter,InsertChange *
                        \ if v:insertmode == 'i' |
                        \   silent execute '!echo -ne "\e[5 q"' |
                        \ elseif v:insertmode == 'r' |
                        \   silent execute '!echo -ne "\e[3 q"' |
                        \ endif
                au VimLeave * silent execute '!echo -ne "\e[ q"'
        endif

        set conceallevel=2

        autocmd BufEnter *.tex set concealcursor-=n
        autocmd BufEnter *.txt,*.tex,*.md set spell
    '';
    plugins = with pkgs.vimPlugins;
      let tex-conceal = pkgs.vimUtils.buildVimPlugin {
        name = "tex-conceal";
        src = pkgs.fetchFromGitHub {
          owner = "KeitaNakamura";
          repo = "tex-conceal.vim";
          rev = "master";
          sha256 = "Hr1HXivDgigKV/0jNQsA4ZvwEu/O1uRnTw61PrvD2BY=";
        };
      };
      in let vim-searchindex = pkgs.vimUtils.buildVimPlugin {
        name = "vim-searchindex";
        src = pkgs.fetchFromGitHub {
          owner = "google";
          repo = "vim-searchindex";
          rev = "master";
          sha256 = "gDbzUF6KqBloY4dIzZrjiSq5sMGs09awH+SKTOscMKk=";
        };
      };
      in [
        ale
        auto-pairs
        gruvbox
        nerdcommenter
        nerdtree
        nerdtree-git-plugin
        tabular
        tex-conceal
        vim-airline
        vim-better-whitespace
        vim-cpp-enhanced-highlight
        vim-devicons
        vim-easytags
        vim-misc
        vim-searchindex
        vim-signify
        vim-surround
      ];
  };

  programs.bash = {
    enable = true;
    bashrcExtra = ''
      if [[ "$OSTYPE" == "darwin"* ]]; then
        export BASH_SILENCE_DEPRECATION_WARNING=1
        eval "$(/opt/homebrew/bin/brew shellenv)"
        PATH="/opt/homebrew/opt/coreutils/libexec/gnubin:$PATH"
        alias tex-to-pdf="/Users/andrew/Programming/BASH/tex-to-pdf/tex-to-pdf"
        export LDFLAGS="-L/opt/homebrew/lib"
        alias make=gmake
        export QUTE_CONFIG_DIR="~/.qutebrowser"
        alias gcc="gcc -I/opt/homebrew/include"
        alias tailscale="/Applications/Tailscale.app/Contents/MacOS/Tailscale"
      elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
        alias tex-to-pdf="/home/andrew/Programming/BASH/tex-to-pdf/tex-to-pdf"
      fi

      PS1="\$(
      return_value=\$?;
      printf \"╭╴\033[0;31m<\u@\h>\033[0m-\033[1;34m<\w\a>\033[0m\033[0;33m\033[0m\$( 2>/dev/null git branch | sed -n -e '/* /p' | sed -e 's/\(* \)//g' -e 's/^/\-\[\033[0;35m\]</' -e 's/$/>\[\033[0m\]/' )\"
      [ -z $NIX_BUILD_CORES ] || echo -ne '-\e[0;93m[nix-shell]\033[0m'
      [ \$return_value = 0  ] && echo -e '-\e[0;32m<\xE2\x9C\x93>' || echo -e '-\e[0;31m<X>')\033[0m\n╰╴>"

      PATH="$PATH:~/.cargo/bin"
      alias ls=lsd

      eval "$(dircolors ~/.gruvbox.dircolors)"

      # GPG Agent
      export GPG_TTY=$(tty)
      export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
      gpgconf --launch gpg-agent
      gpg-connect-agent updatestartuptty /bye > /dev/null
      gpg-connect-agent "scd serialno" "learn --force" /bye > /dev/null
    '';
  };

  programs.ssh = {
    enable = true;
    matchBlocks = {
      aur = {
        hostname = "aur.archlinux.org";
        user = "aur";
      };

      gitlab = {
        hostname = "gitlab.com";
        user = "git";
      };

      github = {
        hostname = "github.com";
        user = "git";
      };

      isengard = {
        hostname = "isengard.mines.edu";
        user = "ahett";
      };
    };
  };

  programs.zathura = {
    enable = true;
    options = {
      recolor = true;
      statusbar-home-tilde = true;
      window-title-home-tilde = true;
      statusbar-page-percent = true;
      # Gruvbox Dark Colorscheme
      notification-error-bg       = "#282828";
      notification-error-fg       = "#fb4934"; # bright:red
      notification-warning-bg     = "#282828"; # bg
      notification-warning-fg     = "#fabd2f"; # bright:yellow
      notification-bg             = "#282828"; # bg
      notification-fg             = "#b8bb26"; # bright:green

      completion-bg               = "#504945"; # bg2
      cotion-fg                   = "#ebdbb2"; # fg
      completion-group-bg         = "#3c3836"; # bg1
      completion-group-fg         = "#928374"; # gray
      completion-highlight-bg     = "#83a598"; # bright:blue
      completion-highlight-fg     = "#504945"; # bg2

      # Define the color in index mode
      index-bg                    = "#504945"; # bg2
      index-fg                    = "#ebdbb2"; # fg
      index-active-bg             = "#83a598"; # bright:blue
      index-active-fg             = "#504945"; # bg2

      inputbar-bg                 = "#282828"; # bg
      inputbar-fg                 = "#ebdbb2"; # fg

      statusbar-bg                = "#504945"; # bg2
      statusbar-fg                = "#ebdbb2"; # fg

      highlight-color             = "#fabd2f"; # bright:yellow
      highlight-active-color      = "#fe8019"; # bright:orange

      default-bg                  = "#282828"; # bg
      default-fg                  = "#ebdbb2"; # fg
      render-loading              = true;
      render-loading-bg           = "#282828"; # bg
      render-loading-fg           = "#ebdbb2"; # fg

      # Recolor book content's color
      recolor-lightcolor          = "#282828"; # bg
      recolor-darkcolor           = "#ebdbb2"; # fg
    };
  };

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/andrew/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    EDITOR = "vim";
    XDG_DATA_DIRS = "$HOME/.nix-profile/share:\${XDG_DATA_DIRS}";
    NIX_SHELL_PRESERVE_PROMPT=1;
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Configure services
  services = {
    gpg-agent = if pkgs.system != "aarch64-darwin"
      then {
        enable = true;
        enableSshSupport = true;
        enableScDaemon = true;
        pinentryFlavor = "gtk2";
      }
      else {}; # gpg-agent not supported on macOS
    syncthing = {
      enable = true;
      extraOptions =
        let config_dir = home_dir + "/.config/syncthing"; in
        let data_dir = home_dir + "/Sync/.data"; in
        [
          "--config" config_dir
          "--data" data_dir
        ];
    };
  };
}
